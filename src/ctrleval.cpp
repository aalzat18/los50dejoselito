#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstdio>
#include <stdlib.h>
#include <iostream>
#include <cstring>
#include <unistd.h>
#include <vector>
#include <regex>
#include <fstream>

using namespace std;

int numeroEval;
vector<pthread_mutex_t> mtxHilos;
string entorno;
pthread_mutex_t mtxEntorno;
string ruta_total;
vector<string> idEvaluador;


struct WriteIn {
    int in;
    int out;
};

struct InfoProceso{
    int idProceso;
    string rutaFichero;
    string nombreFichero;
};

void* expresionRegular(const char* arg){
    string num     = "([1-9][0-9]∗|0)";
    string id = "[a-zA-Z]([a-zA-Z]|[0-9])*";
    //Control ID { (./ID/)+ID ID.cfg }
    regex expreCtrleval("Evaluador");
    regex numero(num);
    string linea;
    ifstream ctrleval_cfg;
    ctrleval_cfg.open(arg);
    while(getline(ctrleval_cfg,linea)){
        //Si la linea cumple con la expresion es un eval
        if(regex_search(linea,expreCtrleval)){
            numeroEval++;
            string aux  = linea.substr(0,linea.find("{"));
            string aux1 = aux.substr(aux.find(" ")+1);
            idEvaluador.push_back(aux1);
        }
    }
    ctrleval_cfg.close();
}

void* hiloControl(void* informacion){
    int* aux = (int*) informacion;
    //Se obtiene el numero del proceso
    int numProceso = *aux;
    WriteIn wiFile;
    int pipeInEval[2];
    int pipeOutEval[2];
    pipe(pipeInEval);
    pipe(pipeOutEval);

    pid_t proceso;
    if((proceso = fork()) == 0){
        close(pipeInEval[1]);
        close(pipeOutEval[1]);
        //dup2(pipeOutEval[0], 1);
        dup2(pipeInEval[0], 0);
        execl("./evaluador", "evaluador", ruta_total.c_str(), idEvaluador[numProceso].c_str() ,NULL);
    }else{
        close(pipeOutEval[0]);
        close(pipeInEval[0]);
        wiFile.out = pipeOutEval[1]; //Entrada estandar
        wiFile.in = pipeInEval[0]; //Salida estandar
    }
    while(true){
    //Se espera la orden para imprimir
      pthread_mutex_lock(&(mtxHilos[numProceso]));
      //cout << "Despierto" << endl;
    //Se le ordena a el proceso para que escriba
      if(write(pipeInEval[1], entorno.c_str(), entorno.size()) <= 0){
        //Hay error en la escritura
        break;
      }
      //Espera la salida del evaluador
      //Desbloquea el mutex
      pthread_mutex_unlock(&mtxEntorno);
   }
    return NULL;
}


int
main(int argc, char *argv[], char *envp[]){

    vector<pthread_t> hilosEntrada;
    pthread_mutex_init(&mtxEntorno, NULL);
    string ruta, nombre;
    char** env;
    for (env = envp; *env != 0; env++){
        char* thisEnv = *env;
        string variable(thisEnv);
        string nombreVariable = variable.substr(0, variable.find("="));
        if(nombreVariable == "DIRDETRABAJO"){
            ruta = variable.substr(variable.find("=")+1) +"/";
        }
        if(nombreVariable == "FICHEROCFG"){
            nombre = variable.substr(variable.find("=")+1);
        }
    }
    if(ruta.empty() or nombre.empty()){
        cerr << "No estan las variables definidas" << endl;
        exit(1);
    }
    ruta_total = ruta+nombre;

    expresionRegular(ruta_total.c_str());
    //inicializa los hilos
    for(int i = 0; i < numeroEval; i++){
        pthread_t entrada;
        pthread_mutex_t mtxHilo;
        hilosEntrada.push_back(entrada);
        mtxHilos.push_back(mtxHilo);
    }
    //crea los hilos
    for(int i = 0; i < numeroEval; i++){
        pthread_mutex_init(&(mtxHilos[i]), NULL);
        pthread_mutex_lock(&(mtxHilos[i]));
        int* id = new int(i);
        pthread_create(&(hilosEntrada[i]), NULL, hiloControl,(void*) id); //El hilo que envia a mayus
    }

    string a;
    while(getline(cin, a)){
        entorno = a + "\n";
        pthread_mutex_lock(&mtxEntorno);
        pthread_mutex_unlock(&(mtxHilos[0]));
        //Esperar a que termine el hilo
        pthread_mutex_lock(&mtxEntorno);

    }

    printf("me voy a morir\n");
    return 0;
}
