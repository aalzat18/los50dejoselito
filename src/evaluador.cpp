#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <cstdio>
#include <set>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cctype>


using namespace std;


string ss;

vector<string> ecuaciones;
vector<char*> variables;

//Recibi cfg para parsear
void separar(const char* archivo, const char* id){

    string linea;
    ifstream ctrlsis_cfg;
    ctrlsis_cfg.open(archivo);
    while(getline(ctrlsis_cfg,linea)){
      if(linea.find("{") < linea.size()){
        string aux  = linea.substr(0,linea.find("{"));
        if(!aux.empty()){
            string aux1 = aux.substr(aux.find(" ")+1);
            int idEnt = atoi(id);
            int auxEnt = stoi(id);
            if(idEnt == auxEnt){
                while(getline(ctrlsis_cfg,linea)){
                    if(linea=="}"){
                        break;
                    }
                    ss = ss + linea;
                }
                break;
            }
        }
      }
    }
    char *expresion, *variable, *saveptr_expresion, *saveptr_variable;
    char* dump_archivo = strdup(ss.c_str());
    expresion = strtok_r(dump_archivo, ";", &saveptr_expresion);
    while(expresion != NULL){
        string formula(expresion);
        //cout << formula << endl;
        ecuaciones.push_back(formula);
        //Lo mando para bc
        variable = strtok_r(expresion, "=", &saveptr_variable);
        //Guardo la variable
        variables.push_back(variable);
        //printf("Variable: %s\n", variable);
        expresion = strtok_r(NULL, ";", &saveptr_expresion);
    }
    ctrlsis_cfg.close();
}

//The popen() function executes a command and pipes the executes command to the calling program,
// returning a pointer to the stream which can be used by calling program to read/write to the pipe.
string exec(const char* cmd) {
    array<char, 128> buffer;
    string result;
    shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
    if (!pipe) throw std::runtime_error("popen() failed!");
    while (!feof(pipe.get())) {
        if (fgets(buffer.data(), 128, pipe.get()) != NULL)
            result += buffer.data();
    }
    return result;
}

int main(int argc, char *argv[]){
    printf("evaluando\n");
    vector<string> resultados;
    separar(argv[1], argv[2]);
    string command;
    string variablesEvaluar;
    string entorno;
    string salida;
    string entrada;
    cout << ecuaciones.size() << endl;
    while(getline(cin, entrada)){
        entorno = entrada;
        string auxiliar = entorno.substr(entorno.find("{")+1, entorno.find("}")-1);
        //Se toma el entorno y se evalua
        for(int i=0, j=0; i < ecuaciones.size(), j < variables.size(); i++, j++){
            //cout << ecuaciones[i] << endl;
            variablesEvaluar = "{" + auxiliar + ecuaciones[i] + ";" + variables[j] +";}";
            command = "echo \"{" + string(variablesEvaluar) + "}\" | bc";
            string result_bc = exec(command.c_str());
            auxiliar = auxiliar + variables[j] + "=" + result_bc + ";";
            //cout << result_bc << endl;
            resultados.push_back(result_bc);
        }

        for(int i=0; i<resultados.size(); i++){
            salida = salida + variables[i] + "=" + resultados[i];
        }
        salida.erase(remove(salida.begin(), salida.end(), '\t'), salida.end());
        salida.erase(remove(salida.begin(), salida.end(), ' '), salida.end());
        cout << salida << endl;

        //cout << entrada << endl;
    }
    return 0;
}
