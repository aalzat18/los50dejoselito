#include <iostream>
#include <unistd.h>
#include <fstream>
#include <vector>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstdio>
#include <regex>
#include <string>
#include <string.h>


using namespace std;

struct WriteIn {
  int in;
  int out;
};

struct InfoProceso{
    int idProceso;
    string rutaFichero;
    string nombreFichero;
};

int numeroProcesos = 0;
pthread_mutex_t mtxSalida;
vector<pthread_mutex_t> mtxHilos;
vector<InfoProceso> infoVector;
//vector donde se guarda el nombre del archivo
vector<string> rutaArchivos;
vector<string> nombreArchivos;
string entorno;

void* expresionRegular(const char* arg){
    string punto   =".";
    string punto2  = "..";
    string evalCfg = ".cfg";
    string ID = "[a-zA-Z]([a-zA-Z]|[0-9])*";
    //Control ID { (./ID/)+ID ID.cfg }
    regex expreCtrlsis("Control[[:s:]]"
        +ID+"[[:s:]]?\\u007B[[:s:]]?(("
        +punto+"|"+punto2+")\\u002F)+"+ID+"(\\u002F"
        +ID+")*[[:s:]]"+ID+evalCfg+"[[:s:]]?\\u007D");

    regex nombre("[a-zA-Z]([a-zA-Z]|[0-9])*"+evalCfg);
    regex path("((\\u002E|"+punto2+")\\u002F)+"
      +ID+"(\\u002F"+ID+")*");

    string linea;
    ifstream ctrlsis_cfg;
    ctrlsis_cfg.open(arg);
    while(getline(ctrlsis_cfg,linea)){
        //Si la linea cumple con la expresion es un proceso
        smatch resul;
        if(regex_search(linea,resul,expreCtrlsis)){
            numeroProcesos++;
            string resultado = resul.str();

            //Encontrar el nombre del archivo
            smatch nombreArchivo;
            smatch rutaArchivo;
            if(regex_search(resultado,nombreArchivo,nombre));
            if(regex_search(resultado,rutaArchivo,path));
            rutaArchivos.push_back(rutaArchivo.str());
            nombreArchivos.push_back(nombreArchivo.str());
        }

    }
    ctrlsis_cfg.close();
}


void* hiloControl(void* informacion){
    InfoProceso* info = (InfoProceso*) informacion;

    string DIRDETRABAJO = "DIRDETRABAJO="+info->rutaFichero;
    string FICHEROCF = "FICHEROCFG="+info->nombreFichero;
    char const* env_vars[] = { DIRDETRABAJO.c_str(), FICHEROCF.c_str(),(char *) NULL};

    int valor = info->idProceso;
    int pipeInEval[2];
    int pipeOutEval[2];
    pipe(pipeInEval);
    pipe(pipeOutEval);

    pid_t proceso;
    if((proceso = fork()) == 0){
      close(pipeInEval[1]);
      close(pipeOutEval[1]);
      //dup2(pipeOutEval[0], 1);
      dup2(pipeInEval[0], 0);//crear hilo hacia salida estandar
      execle("./ctrleval", "ctrleval", (char *) NULL, env_vars);
    }else{
      close(pipeOutEval[0]);
      close(pipeInEval[0]);
      WriteIn wiFil, wiEval;
      wiFil.out = pipeOutEval[1]; //Entrada estandar
      wiFil.in = pipeInEval[0]; //Salida estandar
    }
    //Revisar la entrada
    while(true){
      //Se espera la orden para imprimir
      pthread_mutex_lock(&(mtxHilos[valor]));
      //Se le ordena a el proceso para que escriba
      if(write(pipeInEval[1], entorno.c_str(), entorno.size()) <= 0){
        printf("Hay error\n");
        //Hay error en la escritura
        break;
      }
      //Esperar a que le impriman
   }

}

int
main(int argc, char *argv[]) {

  vector<pthread_t> hilos;
  entorno = "a=1; b=10; c=2;\n";
  expresionRegular(argv[1]);
  //incializar los hilos
  for(int i = 0; i < numeroProcesos; i++){
    pthread_t hilo;
    pthread_mutex_t mtxHilo;
    hilos.push_back(hilo);
    mtxHilos.push_back(mtxHilo);
    int* id = new int(i);
    InfoProceso informacion = {int(i),string(rutaArchivos[i]),string(nombreArchivos[i])};
    infoVector.push_back(informacion);
  }
  //crea los hilos
  for(int i = 0; i < numeroProcesos; i++){
    pthread_mutex_init(&(mtxHilos[i]), NULL);
    pthread_mutex_lock(&(mtxHilos[i]));
    pthread_create(&(hilos[i]), NULL, hiloControl,(void*) &(infoVector[i]));
  }

  string a="mostrar";
  cin >> a;
  while(a != "parar"){
    if(a == "mostrar"){
      pthread_mutex_unlock(&(mtxHilos[0]));
    }
    cin >> a;
  }

  return 0;
}
